import React from 'react';
import ReactDOM from 'react-dom';

export class App extends React.Component {
	render() {
		return (
			<div>
				<div class="header">
					<div class="logo"><a href=""><img src="app/img/mq-new-logo.png" alt="Merqueo"/></a></div>
					<p class="welcome-client">Hola John</p>
					<img src="app/img/menu.svg" alt="Menu" class="menu-icon"/>
				</div>
				<div class="menu-space"></div>
				<div class="wrapper">
					<div class="box special-box bx-shadow bx-shadow-none">
						<textarea placeholder="Escribe aquí tu estado" class="full-text-area"></textarea>
						<div class="btn-container-right">
							<a href="#" class="btn">Publicar</a>
						</div>
					</div>

					<div class="box mt-20 bx-shadow">
					<div class="coment-main-container">
						<div class="avatar-container">
							<img src="app/img/user.svg" alt="user avatar" />
						</div>
						<div class="comment-container">
							<p class="user-name">John Moreno</p>
							<p class="publication-time">Hace 40 min</p>
							<p class="comment">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
						</div>
						<div class="reaction-box">
								<a href="#" class="reaction-btn no-side-border">Reaccionar</a><a href="#" class="reaction-btn">Comentar</a>
						</div>
						<div class="box no-side-border">
							<div class="reaction-count"><img src="app/img/reactions.jpg" alt="reactions images" /></div>
							<div class="comment-count">3 comentarios</div>
						</div>
						</div>
						<div class="box noborder bg-gray comment-box-group">
							<div class="comment-box-item">
								<div class="avatar-container">
									<img src="app/img/user.svg" alt="user avatar" />
								</div>
								<div class="comment-container">
									<p class="user-name">Andrés Velandia <span class="publication-time">Hace 40 min</span></p>
									<p class="comment">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
								</div>
							</div>
							<div class="comment-box-item">
								<div class="avatar-container">
									<img src="app/img/user.svg" alt="user avatar" />
								</div>
								<div class="comment-container">
									<p class="user-name">Fausto Amaya <span class="publication-time">Hace 40 min</span></p>
									<p class="comment">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
								</div>
							</div>
							<div class="comment-box-item">
								<div class="avatar-container">
									<img src="app/img/user.svg" alt="user avatar" />
								</div>
								<div class="comment-container">
									<p class="user-name">Ingrith Hincapie <span class="publication-time">Hace 40 min</span></p>
									<p class="comment">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
								</div>
							</div>
							<div class="wrapper">
								<textarea placeholder="Escribe un comentario"></textarea>
								<div class="btn-container-right">
									<a href="#" class="btn">Publicar</a>
								</div>
							</div>
						</div>

					</div>
					<div class="box mt-20 bx-shadow">
					<div class="coment-main-container">
						<div class="avatar-container">
							<img src="app/img/user.svg" alt="user avatar" />
						</div>
						<div class="comment-container">
							<p class="user-name">Liliana Jimenez</p>
							<p class="publication-time">Hace 40 min</p>
							<p class="comment">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
						</div>
						<div class="reaction-box no-side-border no-down-border">
								<a href="#" class="reaction-btn">Reaccionar</a><a href="#" class="reaction-btn">Comentar</a>
						</div>
						<div class="box">
							<div class="reaction-count"><img src="app/img/reactions.jpg" alt="reactions images" /></div>
							<div class="comment-count"></div>
						</div>
						</div>
					</div>
				</div>
</div>
		);
	}
}

ReactDOM.render(
	<App/>, 
	document.getElementById('app')
);
