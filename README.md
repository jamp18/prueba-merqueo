#John Alexander Moreno Parra
######jamp9118@gmail.com - 3162426566

##Prueba Merqueo

##Requisitos
* Tener instalado nodejs version 8.\*
* Tener instalado NPM

##Instalación del proyecto
* Descargar repositorio con `git clone https://bitbucket.org/jamp18/prueba-merqueo.git`
* Dentro de los comandos realizar npm install

##Ejecutar proyecto
 * npm run build
 * npm run start
 * Abrir en el explorador la ruta `http://localhost:8080/`